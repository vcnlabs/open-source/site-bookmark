# Add to Homescreen call out

Script for mobile devices, it automatically shows an overlaying message encouraging to add the web app to the homescreen. Compatible with iOS 6+ and Chrome for Android (soon WinPhone).

## Installation

Add `addtohomescreen.css` and `addtohomescreen.js` to the head of your projects index file.

Then, call the script in the body.

For example:

```html
<head>
...
<link rel="stylesheet" type="text/css" href="../../style/addtohomescreen.css">
</head>
<body>
<script src="src/addtohomescreen.js"></script>
<script>
var ath = addToHomescreen({          // activate debug mode in ios emulation
    skipFirstVisit: false,  // show at first access
    startDelay: 0,          // display the message right away
    lifespan: 0,            // do not automatically kill the call out
    displayPace: 0,         // do not obey the display pace
    maxDisplayCount: 0      // do not obey the max display count
});
</script>  
</body>
```

## Configuration

###### "skipFirstVisit:"

true, never show at first access (ie., show popup only for RETURNING users.)  
false, always show at first access


###### "startDelay:" 

0, display the message right away  
5, display the message 5 seconds later


###### "lifespan:"

0, do not automatically kill the call out  
5, close the message after 5 seconds


###### "displayPace:"

0, shows the message every time the page is visited  
1440, only show the message every 1440s


###### "maxDisplayCount:"
0, no limit on maximum times the message is shown  

<br />

--------------------------------------

###### Other options and explanations taken from [project website](http://cubiq.org/add-to-home-screen):  

modal: prevents further actions on the website until the message is closed.

mandatory: the website is not accessible until the user adds the application to the homescreen. This is useful mainly for web-app-capable applications that need full screen viewing to be accessed (eg: games).

skipFirstVisit: setting this to true prevents the message to appear the first time the user visits your website. It is highly recommended to enable this option!

startDelay: seconds to wait from page load before showing the message. Default: 1.

lifespan: seconds to wait before automatically close the message. Default: 15 (set to 0 to disable automatic removal).

displayPace: minutes before the message is shown again. By default it’s set to 1440, meaning that we will be showing the message only once per day.

maxDisplayCount: absolute maxium number of times the call out will be shown. (0 = no limit).

icon: display the touch icon in the pop up message. Default: true

message: you can provide your custom message if you don’t like the default.

onShow: custom function executed when the message is shown.

onAdd: custom function executed when the application is added to the homescreen. Please note that this is a guesstimate (see below).

detectHomescreen: ATH tries to detect the homescreen. Supported values are: false, true (=’hash’), ‘hash’, ‘queryString’, ‘smartURL’ (see below).

autostart: the message is not shown automatically and you have to trigger it programmatically.

<br />
<br />

## License

Copyright (c) 2014 Matteo Spinelli, http://cubiq.org/

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
